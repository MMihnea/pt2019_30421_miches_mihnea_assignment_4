package Business;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author Miches
 *	The order class that will map to a List of MenuItems
 */
public class Order {
	public Date date;
	public int orderID;
	/**
	 * @param id unique id of the order
	 */
	public Order(int id) {
		this.orderID = id;
		this.date = new Date();
	}
	
	/* 
	 * Return the unique ID as hash code
	 */
	public int hashCode() {
		return this.orderID;
	}
	
	public String toString() {
		return this.orderID + " " + this.date.toString();
	}
	
}
