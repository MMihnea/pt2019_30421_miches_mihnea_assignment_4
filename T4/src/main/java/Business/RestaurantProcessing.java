package Business;
import java.util.List;
import java.util.Map;

public interface RestaurantProcessing {
	
	/**
	 * @return the whole menu's list
	 */
	public List <MenuItem> getMenuItems();
	/**
	 * @param index index from ComboBox that identifies the item to be returned
	 * @return a component as requested by the index parameter
	 */
	public MenuItem getItemAtIndex(int index);
	/**
	 * @param mi item to be added to the menu
	 */
	public void addItem(MenuItem mi);
	/**
	 * @param index index of item to be removed coming from ComboBox
	 */
	public void removeItemAtIndex(int index);
	/**
	 * @param index index of item to be edited coming from ComboBox
	 * @param name new name
	 * @param price new price if possible
	 */
	public void editItemAtIndex(int index, String name, float price);
	/**
	 * @param o order to be added to map
	 * @param items the list of items in the order
	 */
	public void addOrder(Order o, List <MenuItem> items);
	/**
	 * @return the map of orders
	 */
	public Map <Order, List <MenuItem>> getOrders();
	/**
	 * @param index index of order for which to compute price coming from ComboBox
	 * @return the total price of order
	 */
	public float computePrice(int index);
	/**
	 * @param index index of order to be exported coming from ComboBox
	 */
	public void export(int index);
	/**
	 * @return the maximum index to be added to the orders map, is unique
	 */
	public int getMaxIndex();
	/**
	 * Deserialize the menu file
	 */
	public void importMenu();
	/**
	 * Serialize the menu file
	 */
	public void exportMenu();
	
}
