package Business;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Miches
 *This is the Composite in the Composite Design Pattern and it contains
 *a List of Components
 */
public class CompositeProduct implements MenuItem, Serializable{
	
	public List<MenuItem> items = new ArrayList<MenuItem>();
	public String name;
	
	/**
	 * @param name name of the product
	 */
	public CompositeProduct(String name) {
		this.name = name;
	}
	
	/*
	 * Compute price as sum of prices of components
	 */
	public float computePrice() {
		float price = 0;
		
		for(MenuItem item: items) {
			price += item.computePrice();
		}
		
		return price;
	}
	
	/**
	 * Add a new component to the List
	 * @param mItem new component to add
	 */
	public void addComponentProduct(MenuItem mItem) {
		this.items.add(mItem);
	}
	
	public String toString() {
		return this.name + " " + this.computePrice();
	}
}
