package Business;

/**
 * @author Miches
 *This is the Component interface.
 */
public interface MenuItem {
	public float computePrice();
}
