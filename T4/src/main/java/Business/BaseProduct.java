package Business;
import java.io.Serializable;

/**
 * @author Miches
 *This class implements the BaseProduct, a leaf in the Composite Design Pattern
 */
public class BaseProduct implements MenuItem, Serializable{
	
	public String name;
	public float price;
	
	/**
	 * @param name name of the product
	 * @param price price of the product
	 */
	public BaseProduct(String name, float price) {
		this.name = name;
		this.price = price;
	}
	
	public float computePrice() {
		return this.price;
	}
	
	public String toString() {
		return this.name + " " + this.price;
	}
}
