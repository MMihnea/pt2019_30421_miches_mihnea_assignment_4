package Data;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import Business.MenuItem;
import Business.Order;

/**
 * @author Miches
 *This class is used to export the bill for an order
 */
public class Exporter {
	/**
	 * @param index index of the order to be exported
	 * @param price price of order
	 * @param o order to be exported itself
	 * @param items list of items in the exported order
	 */
	public void export(int index, float price, Order o, List <MenuItem> items) {
		if(index < 0 || price < 0 || o == null || items == null)
			throw new IllegalArgumentException();
		
		
		String fileName = new String();
		fileName = ("Order" + "-" + o.orderID + ".txt");
		try {
			PrintWriter writer = new PrintWriter(fileName, "UTF-8");
			writer.println("Order placed at:  " + o.date);
			Iterator <MenuItem> itProd = items.iterator();
			writer.println("Contents: ");
			while(itProd.hasNext())
				writer.println(itProd.next().toString());
			writer.println("Price:   " + price);
			writer.println("Finalized at:   " + new Date());
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		
	}
}
